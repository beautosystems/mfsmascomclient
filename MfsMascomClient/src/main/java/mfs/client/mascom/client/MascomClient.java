package mfs.client.mascom.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

public class MascomClient {

	private static String baseUrl="http://localhost:8080/MfsMascomWeb/";
	
	public static void main(String[] args) {

		MascomClient mascomClient = new MascomClient();

		String response = null;

		String[] jsonResponseParams = new String[3];

		String params = "";
		
		

		Map<String, String> jsonResponse = new HashMap<String, String>();

		int choice = Integer.parseInt(args[0]);

		try {
			switch (choice) {
			case 1:
				response = mascomClient.authorizationService();
				jsonResponse = mascomClient.jsonToMapObject(response);

				if (jsonResponse != null && response.contains("access_token")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = jsonResponse.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 2:
				response = mascomClient.viewAccountType(args[1]);
				jsonResponse = mascomClient.jsonToMapObject(response);

				if (jsonResponse != null && response.contains("accountMsisdn")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = jsonResponse.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 3:
				response = mascomClient.payment(args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8],
						args[9], args[10]);
				jsonResponse = mascomClient.jsonToMapObject(response);

				if (jsonResponse != null && response.contains("mfsTransactionId")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = jsonResponse.get("mfsTransactionId");

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 4:
				response = mascomClient.getSalesOrderDetails(args[1]);
				jsonResponse = mascomClient.jsonToMapObject(response);

				if (jsonResponse != null && response.contains("mfsTransactionId")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = jsonResponse.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
			}
		} catch (Exception e) {
			jsonResponseParams[0] = "02";
			jsonResponseParams[1] = "Fail";
			jsonResponseParams[2] = e.getMessage();
			params = Arrays.toString(jsonResponseParams);
			System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
		}
	}

	private String authorizationService() throws Exception {
		String authorizationTokenServiceUrl = baseUrl+"auth";
		return getConnectionjsonResponseGet(authorizationTokenServiceUrl);
	}

	private String getSalesOrderDetails(String mfsTransactionId) throws Exception {

		String getSalesOrderDetailsServiceUrl = baseUrl+"salesOrder";
		JsonObject getSalesOrderDetailsServiceRequest = new JsonObject();

		JsonObject function = new JsonObject();
		function.addProperty("mfsTransactionId", mfsTransactionId);
		getSalesOrderDetailsServiceRequest.add("function", function);
		return getConnectionjsonResponse(getSalesOrderDetailsServiceUrl, getSalesOrderDetailsServiceRequest.toString());

	}

	private String payment(String msisdn, String remittanceAmount, String brandId, String forexRate,
			String senderAmountCurrency, String senderAccountNumber, String senderAmount, String mfsTransactionId,
			String mfsSystemTransactionNumber, String checkOnly) throws Exception {
		
		String paymentServiceUrl = baseUrl+"payment";
		JsonObject function = new JsonObject();
		
		function.addProperty("msisdn", msisdn);
		function.addProperty("remittanceAmount", remittanceAmount);
		function.addProperty("brandId", brandId);
		function.addProperty("forexRate", forexRate);
		function.addProperty("senderAmountCurrency", senderAmountCurrency);
		function.addProperty("senderAccountNumber", senderAccountNumber);
		function.addProperty("senderAmount", senderAmount);
		function.addProperty("mfsTransactionId", mfsTransactionId);
		function.addProperty("mfsSystemTransactionNumber", mfsSystemTransactionNumber);
		JsonObject paymentServiceRequest = new JsonObject();
		paymentServiceRequest.add("function", function);
		paymentServiceRequest.addProperty("checkOnly", checkOnly);
		
		return getConnectionjsonResponse(paymentServiceUrl, paymentServiceRequest.toString());
	}

	private String viewAccountType(String accountMsisdn) throws Exception {

		String viewAccountTypeServiceUrl = baseUrl+"viewAcountType";
		JsonObject viewAccountTypeServiceRequest = new JsonObject();
		JsonObject function = new JsonObject();
		function.addProperty("accountMsisdn", accountMsisdn);
		viewAccountTypeServiceRequest.add("function", function);
		return getConnectionjsonResponse(viewAccountTypeServiceUrl, viewAccountTypeServiceRequest.toString());
	}

	private String getConnectionjsonResponse(String serviceUrl, String request) throws Exception {

		HttpURLConnection connection = null;

		URL url = new URL(serviceUrl);

		connection = (HttpURLConnection) url.openConnection();

		connection.setRequestMethod("POST");

		connection.setRequestProperty("Content-type", "application/json");
		connection.setRequestProperty("Accept", "application/json");
		connection.setUseCaches(false);
		connection.setDoOutput(true);
		connection.setConnectTimeout(20000);

		DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
		wr.writeBytes(request);
		wr.close();

		InputStream is = connection.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader rd = new BufferedReader(isr);
		StringBuilder jsonResponse = new StringBuilder();
		String line;

		while ((line = rd.readLine()) != null) {
			jsonResponse.append(line);
			jsonResponse.append('\r');
		}

		rd.close();

		if (connection != null) {
			connection.disconnect();
		}

		return jsonResponse.toString();
	}

	Map<String, String> jsonToMapObject(String jsonString) throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		Map<String, String> map = new HashMap<String, String>();

		map = mapper.readValue(jsonString, new TypeReference<Map<String, String>>() {
		});

		return map;
	}

	private String getConnectionjsonResponseGet(String serviceUrl) throws Exception {

		HttpURLConnection connection = null;

		URL url = new URL(serviceUrl);
		connection = (HttpURLConnection) url.openConnection();

		connection.setRequestMethod("GET");

		connection.setUseCaches(false);
		connection.setDoOutput(true);
		connection.setConnectTimeout(20000);

		InputStream is = connection.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader rd = new BufferedReader(isr);
		StringBuilder jsonResponse = new StringBuilder();
		String line;

		while ((line = rd.readLine()) != null) {
			jsonResponse.append(line);
			jsonResponse.append('\r');
		}

		rd.close();

		if (connection != null) {
			connection.disconnect();
		}

		return jsonResponse.toString();
	}

}
